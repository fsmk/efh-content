import imaplib, itertools, csv, collections, re

#This script parses through all the emails in the inbox that ae flagged AND are from the Formspree Team to generate a csv file containing all the details. This file can later be dragged into ethercalc for actual editing.

def remove_values_from_list(the_list, val):
   return [value for value in the_list if value != val]

M = imaplib.IMAP4_SSL('IP.ADDR.OF.YOUR.MAILSERVER.HOSTING', 'PORTNUMBER')
M.login('your.smtp.login.thing', 'your.password')
M.select()
typ, data = M.search(None, 'FROM', '"Formspree Team"', 'FLAGGED')

# The parameters in this email are based on the application form at embeddedforher.com/apply

form_headers = ('name:', 'email:', 'mobile:', 'resume:', 'tech-project:', 'why-participate:', 'what-achieve:', 'what-project:', 'linux-env:')

with open('participants.csv', 'wb') as csvfile:
    filewriter = csv.writer(csvfile, delimiter=' ',quotechar=',', quoting=csv.QUOTE_MINIMAL)
    for fh in form_headers
        filewriter.writerow(fh)
    for num in data[0].split():
        typ, data = M.fetch(num, '(RFC822.TEXT)')
        flag=10000
        # dict1 = collections.OrderedDict()
        # print data[0][1].split("\n\r")
        for d in data[0][1].split("\n\r"):
            list1 = []
            for num2, fh in zip(range(len(form_headers)), form_headers):
                if fh in d:
                    # print d.encode('string_escape')
                    # pat = re.compile('*\r')
                    list1.append(d.split("\r")[1].strip('\r'))
                    # print list1[num2].encode('string_escape')
                    # raw_input()
                    # print num2
                    # list1.append(d.split("\r\n")[1].split("\r")[0])
                else:
                    list1.append(' ')
            # if dict1[fh]:
                # for fh in form_headers:
            list1 = remove_values_from_list(list1, ' ')
            filewriter.writerow(list1)




M.close()
M.logout()
