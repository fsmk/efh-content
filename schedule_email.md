Dear participants,

A warm welcome to you all from Free Software Movement Karnataka.

Firstly, we're sure you are just as eager to get to know each other as we are to know you. It's about time that we form an IM group. Just like [the rest of FSMK][7], we suggest using [Riot][8]. After creating your user in your preferred app, please join the group [here][9].

Secondly, please find the schedule and locations of the ten sessions below.

Date        Location
10th June   [Workbench Projects][1]
17th June   [LatLong][2]
24th June   [Black Pepper][3]
01st July   [Bangalore Alpha Labs][4]
08th July   [Black Pepper Technologies][3]
15th July   [Workbench Projects][1]
22nd July   [IKP Eden][5]
29th July   [Bangalore Alpha Labs][4]
05th Aug    [LatLong][2]
12th Aug    [HasGeek][6]

If you have any questions, feel free to reach out to any one of us as usual.

Happy chatting!

EmbeddedForHer Team
Free Software Movement Karnataka


[1]:http://workbenchprojects.com/
[2]:http://latlong.in/
[3]:http://www.blackpeppertech.com/
[4]:http://www.bangalorealphalab.in/
[5]:https://ikpeden.com
[6]:http://hasgeek.com/
[7]:https://discuss.fsmk.org/t/fsmk-has-migrated-to-matrix/734
[8]:https://about.riot.im/
[9]:#efh:matrix.org
