Dear Applicant,

Your application for admission in the EmbeddedForHer program conducted by fsmk.org has been given serious consideration by our team. This is to notify you that your application to our program for this iteration has been put on waiting list. We will confirm your seat by the 30th of May, 2017.

In view of the large number of qualified applicants and the limited number of spaces available, our team must be selective. This process is one of identifying those applicants who we believe are qualified with a basic understanding of the subject and exhibit a clear initiative in building projects and driving the community around embedded systems. The answers to our essay questions and resume have all been taken into consideration during the team's review process.

Although we are unable to act completely favourably upon your application, we appreciate your interest in our program. While we are not sure if you will be able to participate in the program itself, we urge you to take a step forward and engage with our community. You are welcome to the FSMK office on Sundays and Public Holidays, and to contact any member of our team or anyone from our host organisation for any assistance of any kind. Please hang out with us on our

Discourse forum: https://discuss.fsmk.org
Riot group: https://riot.im/app/#/room/#fsmk-discuss:matrix.org
Mailing list: http://lists.fsmk.in/listinfo/fsmk-discuss

We hope you'll reach out to us!

EmbeddedForHer Team
Free Software Movement Karnataka
