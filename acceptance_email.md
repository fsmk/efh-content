Dear Applicant,

After careful consideration, we are pleased to announce that your admission to the EmbeddedForHer workshop has been approved. We have gone through your answers to our essay questions and your resume and we will be privileged to have you join us.

In view of the large number of qualified applicants and the limited number of spaces available, our team must be selective. This process is one of identifying those applicants who we believe are qualified with a basic understanding of the subject and exhibit a clear initiative in building projects and driving the community around embedded systems.

The workshop will be held every Saturday from the 10th of June to the 12th of August, 2017 at various locations across Bangalore. A detailed schedule will be emailed to you at a later date.

Your admission will be finalized upon
  - a reply to this email affirming your participation
  - a payment of Rs. 5000/- only in favor of
      Name: Free Software Movement Karnataka
      SB Account No: 1172101012320
      IFSC code :  CNRB0001172
      Bank:  Canara Bank, Kalasipalya. Bangalore

To facilitate the smooth flow of the workshop, we request that you do both tasks at the latest by the 28th of May, 2017, failing which we will be forced to give your seat to those on our waiting lists.

We hope to see you soon!

EmbeddedForHer Team
Free Software Movement Karnataka

Discourse forum: https://discuss.fsmk.org
Riot group: https://riot.im/app/#/room/#fsmk-discuss:matrix.org
Mailing list: http://lists.fsmk.in/listinfo/fsmk-discuss
