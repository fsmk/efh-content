
##General feedback

+ What are your opinions on the format of the course?
  - The balance between lectures and hands-on sessions
  - The changing location of the workshop
  - The distribution of time at the workshop

+ Do you feel that the syllabus was
  - Adequete
  - Comprehensive
  - Relevant to the industry
  - other?

+ What is your opinion about the various hardware devices provided? (such as boards, multimeters, sensors, various tools, etc)

+ How did you find the overall environment of the course? This includes the places and the people.
+ What are some things that you think we could have handled better?
+ Would you like to be involved in the next iteration of this program? If yes, how? If not, why not?

+ How engaging was the course was apart from the days of the actual session? What do you think could have been done to make the program more inclusive and engaging?

### Volunteers
- How interactive and approachable were the volunteers?
- How effective and knowledgable were the volunteers?
- How do you think the volunteers can improve in future sessions?


### Week X

Location:
Speaker:
Volunteers: Niranjan, Anup, Prithvi and Ram
Non-technical session:

- How inclusive and comfortable was the location?

- How effectively was the speaker able to convey to you the given topic?
- How prepared do you think the speaker was?
- How interactive and approachable was the speaker?
- How do you think the speaker can improve in future sessions?

- How relevant, interesting, innovative and worth your time was the non-technical session?

###Projects
+ Are you happy with the team building process? If not, how do you recommend it should be handled?
+ Are you happy with the ideation of your project? How do you suggest we improve it?
+ How do you think "we handled the project implementation"? How do you suggest we improve it?


Speaker list:
- PrithviRaj Narendra
- Nardini
- Nirzaree Vadgama
- Kameshwari NK

Volunteer list:
- Niranjan
- Anup
- PrithviRaj Narendra
- Ramasesan  
- Divya (for one session)

### Other technical session
Tanvi Bhakta - Introduction to Git

###Non-technical sessions
10th June Workbench Projects - basic introduction to FOSS by Ram
17th June LatLong - Rameez's intro to free software
24th June Black Pepper Technologies - Interaction with the CTO
01st July Bangalore Alpha Labs - Current trend in wireless connected devices by Mr Ramesh; Gender in workplace by Hidden pockets
08th July Black Pepper Technologies - Data commodification by Ram
15th July Workbench Projects - Aadhaar talk by Arun, Ganesh, Anand; Art with electronics by Aravinth
22nd July IKP Eden - Prototype to products by Mahesh Venkatachalam
29th July Bangalore Alpha Labs - Working with Embedded systems by Aditi Hilbert
05th Aug LatLong - Aruna  - "Opportunities in FOSS"
